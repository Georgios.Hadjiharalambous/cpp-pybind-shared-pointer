cmake_minimum_required(VERSION 3.1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS "-O3")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

set(PYTHON_LIBRARY_DIR "/home/georgiosh/.local/lib/python3.9/site-packages")
set(PYTHON_EXECUTABLE "/usr/bin/python3.9")

project(pybind_shared_ptr)

include_directories("${CMAKE_SOURCE_DIR}/cpp/src")
include_directories("${CMAKE_SOURCE_DIR}/python")

file (GLOB SOURCE_FILES "src/cpp/*.cpp")
file (GLOB PYTHON_FILES "python/*.cpp")

# Set up such that XCode organizes the files
source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCE_FILES} ${PYTHON_FILES} )

include(pybind11.cmake)
pybind11_add_module(pybind_shared_ptr 
	${SOURCE_FILES}
	${PYTHON_FILES}
)

target_link_libraries(pybind_shared_ptr PUBLIC)

install(TARGETS pybind_shared_ptr
  COMPONENT python
  LIBRARY DESTINATION "${PYTHON_LIBRARY_DIR}"
  )