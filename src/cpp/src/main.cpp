#include "human.cpp"
int main(){
    earth::Population p;
    std::shared_ptr<earth::Human> human1 = std::make_shared<earth::Human>(32,"kokos");
    p.add(human1);
    std::shared_ptr<earth::Human> human2 = std::make_shared<earth::Human>(78,"esi");
    p.add(human2);

    for (auto const x:p.humans){
        std::cout << "name=" << x->name_ << " age=" << x->age_ << "\n";
    }
}