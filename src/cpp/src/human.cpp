#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <thread>
#include <bits/stdc++.h>

namespace earth{

class Human {
    public:
    int age_;
    std::string name_;
 Human(int age, std::string name) {
    age_=age;
    name_=name;
 }   

 ~Human(){
    std::cout<<"hello\n";
 }
 std::string Name(){
    return name_;
 }
 int Age(){return age_;}
};

class Population{
    public:
    std::vector<std::shared_ptr<Human>> humans;
    void add(std::shared_ptr<Human> h){
        humans.emplace_back(h);
    }
    void remove_human(std::shared_ptr<Human> h){
        if (const auto item = std::find(humans.begin(), humans.end(), h);
                    item != humans.end()) {
            humans.erase(item);

        }
    }
    void print_all(){
        for (auto const x:humans){
        std::cout << "name=" << x->name_ << " age=" << x->age_ << "\n";
    }
    }
};

}//namespace earth