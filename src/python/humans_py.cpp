#include "../cpp/src/human.cpp"

#include <pybind11/stl.h>

#include <pybind11/pybind11.h>
namespace py = pybind11;
PYBIND11_MODULE(pybind_shared_ptr, mod) {
  mod.doc() = R"pbdoc(
      Recognised module provides the Population object, which accepts the audio data and spits out transcripts. It uses
      with gstkaldi plugin to do the recognition.
      -----------------------
      .. currentmodule:: Population
      .. autosummary::
         :toctree: _generate
         json
  )pbdoc";
    
    py::class_<earth::Human, std::shared_ptr<earth::Human>>(mod, "Human")
    .def(py::init<int, std::string>(), py::arg("age_"), py::arg("name_"))
    .def("Name", &earth::Human::Name)
    .def("Age", &earth::Human::Age);
    

    py::class_<earth::Population>(mod,"Population")
    .def(py::init<>())
    .def("add", &earth::Population::add, py::call_guard<py::gil_scoped_release>())
    .def("remove_human", &earth::Population::remove_human, py::call_guard<py::gil_scoped_release>())
    .def("print_all",&earth::Population::print_all);

}