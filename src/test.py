import pybind_shared_ptr as py

h = py.Human(21,"kokoko")
h1 = py.Human(2154,"esi kokomo")

# print(h.name)

pop = py.Population()
pop.add(h)
pop.add(h1)
pop.remove_human(h)
del h
print(h1.Name())
pop.print_all()
