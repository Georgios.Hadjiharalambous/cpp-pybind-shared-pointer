# cpp-pybind-shared-pointer

Playing around with pybind from c++ to python.

A simple program to demonstrate how a shared pointer behaves in python land.

Since python uses reference counting, even when we remove an object we need to either delete it explictly (with del) or point the variable to a different object or None. Only then the count of reference is 0 and can be actually delete/destructed in c++ land.

